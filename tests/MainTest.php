<?php

use App\Main;
use PHPUnit\Framework\TestCase;

class MainTest extends TestCase
{
    protected $app;

    public function setUp()
    {
        $this->main = new Main;
    }

    public function test_positive_integer()
    {
        $result = $this->main->solution(123);

        $this->assertSame(132, $result);
    }

    public function test_max_positive_integer_will_not_be_change()
    {
        $result = $this->main->solution(321);

        $this->assertSame(321, $result);
    }

    public function test_positive_integer_include_duplicate_numbers()
    {
        $result = $this->main->solution(5566);

        $this->assertSame(5656, $result);

        $result_2 = $this->main->solution(5566666);

        $this->assertSame(5656666, $result_2);
    }

    public function test_positive_integer_carry()
    {
        $result = $this->main->solution(45321);

        $this->assertSame(51234, $result);
    }

    public function test_negative_integer()
    {
        $result = $this->main->solution(-132);

        $this->assertSame(-123, $result);
    }

    public function test_max_negative_integer_will_not_be_change()
    {
        $result = $this->main->solution(-123);

        $this->assertSame(-123, $result);
    }

    public function test_negative_integer_include_duplicate_numbers()
    {
        $result = $this->main->solution(-7733);

        $this->assertSame(-7373, $result);

        $result_2 = $this->main->solution(-773333);

        $this->assertSame(-737333, $result_2);
    }

    public function test_negative_integer_carry()
    {
        $result = $this->main->solution(-21345);

        $this->assertSame(-15432, $result);
    }

    public function test_when_negative_integer_carry_with_zero_will_not_be_change()
    {
        $result = $this->main->solution(-10234);

        $this->assertSame(-10234, $result);
    }
}
