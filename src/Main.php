<?php

namespace App;

class Main
{
    public function solution(int $input)
    {
        $is_natural_integer = $input >= 0;
        $nums = array_map('intval', str_split(abs($input))); // 123 => [1, 2, 3]
        $num_length = count($nums);

        for ($i=$num_length; $i > 1; $i--) {
            // input 123 => 3-2=1, 1 is positive integer and 123 is positive integer, so execute the swap
            // input -321 => 1-2=-1, -1 is negative integer and -321 is negative integer, so execute the swap
            $cmp = $is_natural_integer
                ? $nums[$i - 1] - $nums[$i - 2] > 0
                : $nums[$i - 1] - $nums[$i - 2] < 0 && $nums[$i - 1] != 0;

            // exchange in head position e.g. 3421 => 4123
            if ($cmp && ($i - 2) === 0) {
                $reversed_tail_integers = array_reverse(array_slice($nums, 2, $num_length));
                $nums = array_merge([$nums[$i - 1]], $reversed_tail_integers, [$nums[$i - 2]]);
                break;
            }

            if ($cmp) {
                [$nums[$i - 1], $nums[$i - 2]] = [$nums[$i - 2], $nums[$i - 1]];
                break;
            }
        }

        $result = intval(implode($nums));

        return $is_natural_integer ? $result : -$result;
    }
}
